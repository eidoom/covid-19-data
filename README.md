# covid-19-data

* Dashboard at <https://www.arcgis.com/apps/opsdashboard/index.html#/f94c3c90da5b4e9f9a0b19484dd4bb14>
* Infection data from <https://www.arcgis.com/home/item.html?id=e5fd11150d274bebaaf8fe2a7a2bda11>
* UK population estimate from <https://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/populationestimates/timeseries/ukpop/pop>

## Disclaimer

This is a bit of fun with maths, not an expert's study!
