#!/usr/bin/env python3
# coding=UTF-8

import csv
import datetime
import re
import argparse

from matplotlib import pyplot
import numpy
from scipy import optimize


class Covid:
    def __init__(self, filename, sub=0):
        self.population = 66435000
        with open(filename) as csvfile:
            data = list(csv.reader(csvfile))[1:]
        size = len(data)
        self.sub = sub
        self.size = size - self.sub
        data = data[: self.size]
        self.verify = int(data[-1][2])
        date, count, cumul = zip(*data)
        p = re.compile(r"(\d{2})/(\d{2})/(\d{4})")
        self.date = [datetime.date.fromisoformat(p.sub(r"\3-\2-\1", d)) for d in date]
        self.count = [int(i) for i in count]
        self.cumul = [int(i) for i in cumul]
        self.x = numpy.arange(self.size)

    def fit(self):
        self.popt, pcov = optimize.curve_fit(self.growth, self.x, self.cumul)

    def ext(self, ext):
        date_ext = [
            self.date[-1] + datetime.timedelta(days=x) for x in range(1, ext + 1)
        ]
        self.date_ext = self.date + date_ext
        x_ext = numpy.arange(self.size, self.size + ext)
        self.x_ext = numpy.concatenate((self.x, x_ext))
        self.extrap = self.growth(self.x_ext, *self.popt)
        print(
            f"Predicted confirmed infected UK population after {ext} days: {100*self.extrap[-1]/self.population}%"
        )

    def plot(self):
        fig, ax = pyplot.subplots()
        ax.scatter(self.date, self.cumul, label="Data")
        ax.plot(self.date_ext, self.extrap, label="Fit", color="red")
        ax.set_xlim([self.date[0], self.date_ext[-1]])
        ax.set_xlabel("Date")
        ax.set_ylabel("Cumulative number of confirmed cases")
        ax.set_title(f"Model {self.num}")
        ax.legend()

    def test(self):
        self.fit()
        self.ext(self.sub)
        print(
            f"Prediction is {self.extrap[-1]/self.verify} of data with {self.sub} day projection"
        )

    def run(self, days):
        self.fit()
        self.ext(days)
        self.plot()

    def banner(self):
        print(f"# Model {self.num}: {'test' if self.sub else 'prediction'}")


class Model0(Covid):
    def __init__(self, filename, sub=0):
        super().__init__(filename, sub)
        self.num = 0
        self.banner()

    def fit(self):
        super().fit()
        print(f"Curve: {self.cumul[0]} * {self.popt[0]}^(days)")

    def growth(self, x, a):
        return self.cumul[0] * numpy.power(a, x)


class Model1(Covid):
    def __init__(self, filename, sub=0):
        super().__init__(filename, sub)
        self.num = 1
        self.banner()

    def fit(self):
        super().fit()
        print(f"Curve: {self.popt[1]} * {self.popt[0]}^(days)")

    def growth(self, x, a, b):
        return b * numpy.power(a, x)


def test_model(model):
    for i in range(3, 0, -1):
        model("DailyConfirmedCases.csv", i).test()


if __name__ == "__main__":
    a = argparse.ArgumentParser()
    a.add_argument("days", type=int)
    days = a.parse_args().days

    test_model(Model0)
    covid0 = Model0("DailyConfirmedCases.csv")
    covid0.run(days)

    test_model(Model1)
    covid1 = Model1("DailyConfirmedCases.csv")
    covid1.run(days)

    pyplot.show()
